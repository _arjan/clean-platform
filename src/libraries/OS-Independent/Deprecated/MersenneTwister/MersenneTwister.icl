implementation module MersenneTwister

import qualified Math.Random as R

genRandReal :: Int -> [Real]
genRandReal n = 'R'.genRandReal n

genRandInt :: Int -> [Int]
genRandInt n = 'R'.genRandInt n
