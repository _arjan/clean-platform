definition module System.Platform

import System._Platform

:: Platform
	= Linux32
	| Linux64
	| Mac
	| Windows32
	| Windows64
